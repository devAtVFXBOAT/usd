# Installing USD (with Houdini plugin)

## macOS - Building USD with houdini plugins enabled

1. Upgrade cmake 
    `brew upgrade cmake`
2. Open the houdini terminal 
    `/Applications/Houdini/Houdini17.0.352/Utilities/Houdini Terminal 17.0.352.app`
3. Nagivate to the houdini toolkit directory
    `cd $HFS/toolkit/`
4. Create a new folder
    `mkdir usd_houdini_plugins_build`
5. Navigate to the recently created folder
    `cd usd_houdini_plugins_build/`
6. Compile the MAKE file 
    `cmake $HFS/toolkit/usd_houdini_plugins`
7. Install the plugin. The -j argument is for parallel operations. Change to the number of cores minus 1 (8 cores, -j7)
    `make -j12 install`
8. Test the plugin with https://graphics.pixar.com/usd/docs/Houdini-USD-Example-Workflow.html
    
### The results for the *step 6* should be something like this:


```   
Marceline:usd_houdini_plugins_build MP_1$ cmake $HFS/toolkit/usd_houdini_plugins
-- The C compiler identification is AppleClang 9.0.0.9000039
-- The CXX compiler identification is AppleClang 9.0.0.9000039
-- Check for working C compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc
-- Check for working C compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++
-- Check for working CXX compiler: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
Installing to /Users/MP_1/Library/Preferences/houdini/17.0
-- Configuring done
-- Generating done
-- Build files have been written to: /Applications/Houdini/Houdini17.0.352/Frameworks/Houdini.framework/Versions/Current/Resources/toolkit/usd_houdini_plugins_build
```    


### The results for the *step 7* should be something like this:



```
Marceline:usd_houdini_plugins_build MP_1$ make -j12 install
Scanning dependencies of target OP_gusd
[ 14%] Building CXX object CMakeFiles/OP_gusd.dir/OBJ_usdcamera.cpp.o
[ 28%] Building CXX object CMakeFiles/OP_gusd.dir/ROP_usdoutput.cpp.o
[ 42%] Building CXX object CMakeFiles/OP_gusd.dir/SOP_usdimport.cpp.o
[ 57%] Building CXX object CMakeFiles/OP_gusd.dir/SOP_usdunpack.cpp.o
[ 71%] Building CXX object CMakeFiles/OP_gusd.dir/OP_Utils.cpp.o
[ 85%] Building CXX object CMakeFiles/OP_gusd.dir/plugin.cpp.o
/Applications/Houdini/Houdini17.0.352/Frameworks/Houdini.framework/Versions/Current/Resources/toolkit/usd_houdini_plugins/ROP_usdoutput.cpp:1101:1: warning: 
      function 'copyKindMetaDataForOverlays' is not needed and will not be emitted [-Wunneeded-internal-declaration]
copyKindMetaDataForOverlays( UsdStageRefPtr stage, SdfPrimSpecHandle p )
^
1 warning generated.
[100%] Linking CXX shared library /Users/MP_1/Library/Preferences/houdini/17.0/dso/OP_gusd.dylib
[100%] Built target OP_gusd
Install the project...
-- Install configuration: ""
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/out
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/out/pixar--usdlayer.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/out/pixar--usdoutput.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/out/pixar--usdreference.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/sop
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/sop/pixar--usdimport.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/sop/pixar--usdinstanceprototypes.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/nodes/sop/pixar--usdunpack.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/UsdOutputOverview.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/help/USDPackedPrims.txt
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/ROP_usdcoalesce.otl
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/ROP_usdlayer.otl
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/ROP_usdreference.otl
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/SOP_camerafrustum.otl
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/SOP_usdbindproxy.otl
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/SOP_usdexportattributes.hda
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/SOP_usdinstanceprototypes.hda
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/otls/SOP_usdretime.hda
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/soho/python2.7/ROP_usdcoalesce.py
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/soho/python2.7/ROP_usdlayer.py
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/soho/python2.7/ROP_usdreference.py
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/python_panels/UsdImport.pypanel
-- Installing: /Users/MP_1/Library/Preferences/houdini/17.0/scripts/obj/pixar-usdcamera.py
```
 

## macOS - Building USD with usdview

### Install xcode
1. Download
    *. macOS 10.3+ https://itunes.apple.com/ar/app/xcode/id497799835?l=en&mt=12

	*. macOs 10.2.6+ https://download.developer.apple.com/Developer_Tools/Xcode_9.2/Xcode_9.2.xip

	*. Other releases https://xcodereleases.com/

2. Copy xcode.app to /Applications/
3. Open xcode.app, accept the licence and wait for the components to be installed.
3. Run in terminal `sudo xcode-select -r`

### Install xcode commandline tools
```
xcode-select --install
```

### Install brew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Install dependencies (Open and close terminal for each line)
Use `brew reinstall xxxxxx` instead of `brew install xxxxxx` to reinstall the packages.

```
brew install gcc cmake boost tbb python2 qt@4
brew install llvm --with-python@2 --with-toolchain
brew install OpenSubdiv OpenEXR
pip install -Iv pyside==1.2.2
pip install -Iv pyopengl==3.1.0
pip install jinja2

```

### Create shortcuts for Houdini SDK
```
cd /Applications/Houdini
mkdir sdk #creates a sdk directory
cd sdk
ln -s  ../Current/Frameworks/Houdini.framework/Versions/Current/Houdini Houdini
ln -s ../Current/Frameworks/Houdini.framework/Versions/Current/Libraries/ Libraries
ln -s ../Current/Frameworks/Houdini.framework/Versions/Current/Resources/toolkit/ toolkit
```

### Clone USD git repo
```
cd ~/Documents/
git clone git@bitbucket.org:devAtVFXBOAT/usd.git
cd usd
```

#### ALTERNATIVE
1. Download [this repo](https://bitbucket.org/devAtVFXBOAT/usd/get/4f66986b7fc3.zip)
2. Unzip in ~/Documents
3. Rename unzipped folder from devAtVFXBOAT-usd-4f66986b7fc3 to usd
4. In terminal
```
cd usd
```

### Install USD
```

sudo mkdir /usr/local/USD
sudo chown $USER /usr/local/USD

python build_scripts/build_usd.py
```

## Issues
###  PySide error
```
ImportError: dlopen(/usr/local/lib/python2.7/site-packages/PySide/QtCore.so, 2): Library not loaded: libpyside-python2.7.1.2.dylib
  Referenced from: /usr/local/lib/python2.7/site-packages/PySide/QtCore.so
  Reason: image not found
```

Add DYLD_LIBRARY_PATH
```
echo "DYLD_LIBRARY_PATH=/usr/local/lib/python2.7/site-packages/PySide" >> ~/.bash_profile
```